package com.energia.demotask.repository;

import com.energia.demotask.domain.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

// PersonRepository class is not required. JPA does it for you

// JpaRepo inherits CRUD methods (create, read, update, delete), Paging and sorting methods
public interface IPersonRepository extends JpaRepository<Person, Integer> {

    // Find by firstName or lastName or email
    @Query("select person from Person person where " +
            "lower(person.firstName) like %:search% or " +
            "lower(person.lastName) like %:search% or " +
            "lower(person.email) like %:search%")
    Page<Person> search(@Param("search") String search, Pageable pageable);
}
