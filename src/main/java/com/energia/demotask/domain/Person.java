package com.energia.demotask.domain;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "person")
public class Person {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "person_id")
    private Integer id;

    @NotBlank(message = "First name is mandatory")
    @Size(min=1, max=64)
    @Column(name = "first_name")
    private String firstName;

    @NotBlank(message = "Last name is mandatory")
    @Size(min=1, max=64)
    @Column(name = "last_name")
    private String lastName;

    @NotBlank(message = "Email is mandatory")
    @Size(min=1, max=64)
    @Column(name = "email")
    private String email;

    @NotBlank(message = "Address is mandatory")
    @Size(min=1, max=64)
    @Column(name = "address")
    private String address;

    @NotBlank(message = "Birth date is mandatory")
    @Size(min=1, max=64)
    @Column(name = "birth_date")
    private String birthDate;


    // ========= Constructors ==========

    public Person() {}

    public Person(String firstName, String lastName, String email, String address, String birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.birthDate = birthDate;
    }


    // ========= ToString =========

    @Override
    public String toString(){
        return String.format(
            "Person [id=%d, firstName=%s, lastName=%s, email=%s, address=%s, birthDate=%s]",
                    id, firstName, lastName, email, address, birthDate
        );
    }


    // ========= Getters and setters =========

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }
}
