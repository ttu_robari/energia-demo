package com.energia.demotask.controller;

import com.energia.demotask.domain.Person;
import com.energia.demotask.repository.IPersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class PersonController {

    private static final Logger LOG = LoggerFactory.getLogger(PersonController.class);

    @Autowired
    private IPersonRepository personRepository;


    // ============== PEOPLE LIST ===============

    // GET
    @GetMapping(value = {"/person", "/"})
    public String getAll(ModelMap model, @RequestParam(value = "search", required = false) String search,
                                         @PageableDefault(sort = "firstName") Pageable pageable)
    {
        if (search == null || search.isEmpty()) {
            model.addAttribute("persons", personRepository.findAll(pageable));
        } else {
            // @Query refuses to convert parameter to lowercase.
            // That's why it needs to be converted before passing it to query
            search = search.toLowerCase();

            model.addAttribute("persons", personRepository.search(
                    search, pageable));
        }

        model.addAttribute("counter", pageable.getPageSize() * pageable.getPageNumber());
        return "person/index";
    }


    // ============== PERSON DETAILS ===============

    // GET
    @GetMapping(value = "/person/details/{id}")
    public String get(Model model, @PathVariable Integer id)
    {
        Optional<Person> person = personRepository.findById(id);
        if (person.isPresent()){
            model.addAttribute("person", person.get());
            return "person/details";
        }

        // Couldn't find person with given id
        return "error";
    }

    // ============== EDIT PERSON ===============

    // GET
    @GetMapping(value = "/person/edit/{id}")
    public String getEdit(Model model, @PathVariable Integer id)
    {
        Optional<Person> person = personRepository.findById(id);
        if (person.isPresent()){
            model.addAttribute("person", person.get());
            return "person/edit";
        }

        // Couldn't find person with given id
        return "error";
    }

    // POST
    @PostMapping(value = "/person/edit/{id}")
    public String postEdit(@Valid Person person, BindingResult bindingResult, @PathVariable Integer id)
    {
        if (bindingResult.hasErrors()){
            return "person/edit";
        } else {
            personRepository.save(person);
            return "redirect:/person";
        }
    }

    // ============== DELETE PERSON ===============

    // GET
    @GetMapping(value = "/person/delete/{id}")
    public String getDelete(Model model, @PathVariable Integer id)
    {
        Optional<Person> person = personRepository.findById(id);
        if (person.isPresent()){
            model.addAttribute("person", person.get());
            return "person/delete";
        }

        // Couldn't find person with given id
        return "error";
    }

    // POST
    @PostMapping(value = "/person/delete/{id}")
    public String postDelete(Model model, @PathVariable Integer id)
    {
        Optional<Person> person = personRepository.findById(id);
        if (person.isPresent()){
            personRepository.delete(person.get());
            return "redirect:/person";
        }

        // Couldn't find person with given id
        return "error";
    }

    // ============== CREATE PERSON ===============

    // GET
    @GetMapping(value = "/person/create")
    public String getCreate(Model model)
    {
        model.addAttribute("person", new Person());
        return "person/create";
    }

    // POST
    @PostMapping(value = "/person/create")
    public String postCreate(@Valid Person person, BindingResult bindingResult)
    {
        if (bindingResult.hasErrors()){
            return "person/create";
        } else {
            personRepository.save(person);
            return "redirect:/person";
        }
    }
}
