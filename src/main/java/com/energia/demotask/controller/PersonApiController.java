package com.energia.demotask.controller;


import com.energia.demotask.domain.Person;
import com.energia.demotask.exception.ApiEntityNotFoundException;
import com.energia.demotask.repository.IPersonRepository;
import com.energia.demotask.util.ApiBadRequestMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api")
public class PersonApiController {

    @Autowired
    private IPersonRepository personRepository;

    // Get list of people
    @GetMapping(value = "/person", produces = "application/json; charset=UTF-8")
    Resources<Resource<Person>> getAll() {
        List<Resource<Person>> person = personRepository.findAll().stream()
                .map(getPerson -> new Resource<>(getPerson,
                        linkTo(methodOn(PersonApiController.class).get(getPerson.getId())).withSelfRel(),
                        linkTo(methodOn(PersonApiController.class).getAll()).withRel("person")))
                .collect(Collectors.toList());

        return new Resources<>(person,
                linkTo(methodOn(PersonApiController.class).getAll()).withSelfRel());
    }

    // Add new person
    @PostMapping("/person")
    Person post(@RequestBody Person person) {
        return personRepository.save(person);
    }

    // Get single person
    // !Note!  produces = "application/json; charset=UTF-8" --> by default browser prefers XML and causes error
    @GetMapping(value = "/person/{id}", produces = "application/json; charset=UTF-8")
    Resource<Person> get(@PathVariable Integer id) {

        Person person = personRepository.findById(id).orElseThrow(() -> new ApiEntityNotFoundException(id));
        return new Resource<>(person,
                linkTo(methodOn(PersonApiController.class).get(id)).withSelfRel(),
                linkTo(methodOn(PersonApiController.class).getAll()).withRel("person"));
    }

    // Update person with given id
    @PutMapping("/person/{id}")
    Person put(@RequestBody Person person, @PathVariable Integer id) {

        return personRepository.findById(id)
                .map(putPerson -> {
                    putPerson.setFirstName(person.getFirstName());
                    putPerson.setLastName(person.getLastName());
                    putPerson.setEmail(person.getEmail());
                    putPerson.setBirthDate(person.getBirthDate());
                    putPerson.setAddress(person.getAddress());
                    return personRepository.save(putPerson);
                })
                .orElseThrow(() -> new ApiEntityNotFoundException(id));
    }

    // Delete person with given id
    @DeleteMapping("/person/{id}")
    void delete(@PathVariable Integer id) {
        Optional<Person> person = personRepository.findById(id);
        if (person.isPresent()) {
            personRepository.delete(person.get());
        } else {
            throw new ApiEntityNotFoundException(id);
        }
    }

    // Handle requests with non existent id's
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ApiEntityNotFoundException.class)
    public ApiBadRequestMessage handleValidationException(ApiEntityNotFoundException e) {
        return new ApiBadRequestMessage(e.getMessage());
    }
}
