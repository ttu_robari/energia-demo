package com.energia.demotask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

@SpringBootApplication
public class DemoTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoTaskApplication.class, args);
    }

    // Add the dialect to the template engine (sorting/ pagination)
    @Bean
    public SpringDataDialect springDataDialect() {
        return new SpringDataDialect();
    }

}
