package com.energia.demotask.util;

import java.sql.Timestamp;

public class ApiBadRequestMessage {

    private Timestamp timestamp;
    private String errorMessage;
    private String errorCode;

    public ApiBadRequestMessage(String message) {
        this.timestamp = new Timestamp(System.currentTimeMillis());
        this.errorCode = "400 Bad Request";
        this.errorMessage = message;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
