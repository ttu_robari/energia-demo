package com.energia.demotask.exception;

public class ApiEntityNotFoundException extends RuntimeException {
    public ApiEntityNotFoundException(Integer id){
        super("Could not find entity with this id ---> " + id);
    }
}
